# Домашка №1, классификация СУБД по CAP теореме. 

## 1) DragonFly

Обратимся к [официальному ресурсу](https://github.com/dragonflydb/dragonfly), процитируем абзац из [документации](https://github.com/dragonflydb/dragonfly/blob/c49e88899b4d1266574e98f2a146c3fca92f8c4d/docs/df-share-nothing.md):

> Dragonfly is a modern replacement for memory stores like Redis and Memcached. It scales vertically on a single instance to support millions of requests per second. It is more memory efficient, has been designed with reliability in mind, and includes a better caching design.

Как видим, мы имеем дело с СУБД, предназначенной к запуску на одном компьютере. Такая СУБД по определению не может обладать качеством Partition Tolerance (если сломается тот самый один компьютер (всего один), то система выйдет из строя полностью).

На всякий случай покажу, что эта СУБД отвечает двум другим требованиям из CAP теоремы. Очевидно, СУБД отвечает требованию Availability, ведь программа запущена только на одной машине. 

Чтобы показать Consistency, процитирую другую часть [документации](https://github.com/dragonflydb/dragonfly/blob/75d35bf987fb810de6ae7fa0f3ba6138656576fc/docs/transaction.md):

> Once the transaction is scheduled, the coordinator starts sending the execution messages. We break each command to one or more micro-ops and each operation corresponds to a single message hop.

> Once a coordinator sends the micro-op request to all the shards, it waits for an answer. Only when all shards executed the micro-op and return the result, the coordinator is unblocked and it can proceed to the next hop. The coordinator is allowed to process the intermediary responses from the previous hops in order to define the next execution request.

Как видим, в структуре этой СУБД присутствует некая сущность, которая отвечает за консистентность данных, какие-либо изменения единовременно происходят на всех "shard"-ах.

Значит DragonFly - пример CA СУБД по CAP теореме.

## 2) ScyllaDB

Сошлюсь на [официальный ресурс](https://www.scylladb.com), конкретно на [документацию](https://www.scylladb.com/product/technology/high-availability/), где разработчики напрямую говорят, что их СУБД относится к категории AP, спорить я с ними не возьмусь.

![](ScyllaDBAP.png)

## 3) ArenadataDB

Согласно [официальному сайту СУБД](https://arenadata.tech/products/arenadata-db/), она обладает консистентностью.

![](ArenadataDBConsistency.png)

Кроме того, создатели СУБД добавили опцию "[segment mirroing](https://docs.arenadata.io/adb/install/adcm/ru/create_cluster/index.html?highlight=отказоустойчивость)", благодаря которой обеспечивается отказоустойчивость.
Они [пишут](https://docs.arenadata.io/adb/index.html#):

> Гибкая система резервирования позволяет развернуть кластер с заранее заданным уровнем отказоустойчивости, позволяя СУБД работать даже при выходе из строя половины серверов из кластера.

Заключаем, что разработчики обеспечивают Partition Tolerance.

Таким образом, имеем пример CP СУБД по CAP теореме.
